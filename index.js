import http from "http";
import fs from "fs";

let status;

try {
  const data = fs.readFileSync("./status");

  status = JSON.parse(data);
} catch (error) {
  console.error("Error reading status file:", error);
  status = { red: 0, blue: 0 };
}

const server = http.createServer((req, res) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Methods", "GET, PUT, OPTIONS");
  res.setHeader("Access-Control-Allow-Headers", "Content-Type");
  res.setHeader("Content-Type", "application/json");

  if (req.method === "OPTIONS") {
    // Handle preflight requests
    res.writeHead(200);
    res.end();
    return;
  }

  if (req.method === "GET") {
    if (req.url === "/red") {
      res.end(JSON.stringify({ red: status.red }));
    } else if (req.url === "/blue") {
      res.end(JSON.stringify({ blue: status.blue }));
    } else if (req.url === "/status") {
      res.end(JSON.stringify(status));
    } else {
      res.statusCode = 404;
      res.end(JSON.stringify({ error: "page not found" }));
    }
    return;
  }

  if (req.method === "PUT") {
    if (req.url === "/red") {
      status.red += 1;
      res.end(JSON.stringify({ red: status.red }));
    } else if (req.url === "/blue") {
      status.blue += 1;
      res.end(JSON.stringify({ blue: status.blue }));
    } else {
      res.statusCode = 404;
      res.end(JSON.stringify({ error: "page not found" }));
    }

    // Save updated status to file
    fs.writeFile("./status", JSON.stringify(status), (error) => {
      if (error) {
        console.error("Error writing status file:", error);
      }
    });
    return;
  }

  res.statusCode = 405;
  res.end();
});

server.listen(3000, () => {
  console.log("Listening to requests on http://localhost:3000");
});

/* 
The OPTIONS method and CORS (Cross-Origin Resource Sharing) are both related to handling cross-origin requests in web applications.

    OPTIONS Method:
    The OPTIONS method is an HTTP method used to determine the communication options available for a particular resource on a web server. When a client sends an OPTIONS request to a server, it expects to receive a response that provides information about the allowed HTTP methods, headers, and other capabilities supported by the server for that resource. This method is often used in combination with other HTTP methods to negotiate the request options before sending the actual request.

    CORS (Cross-Origin Resource Sharing):
    CORS is a mechanism that allows web browsers to make cross-origin requests securely. It is enforced by web browsers to protect users' security and privacy. When a client makes a cross-origin request (i.e., a request from a different domain, protocol, or port), the browser sends a preflight request using the OPTIONS method to the server to check if the actual request is allowed. The server responds with CORS headers, specifying which origins are allowed to access the requested resource.

What they have in common:
Both the OPTIONS method and CORS are used to handle cross-origin requests. The OPTIONS method helps in negotiating the request options with the server, while CORS defines the rules and mechanisms for allowing or restricting cross-origin requests in a secure manner. The OPTIONS method is often used as part of the CORS protocol to check the server's access control policies before making actual cross-origin requests.
*/