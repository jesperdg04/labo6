import './style.css'
import javascriptLogo from './javascript.svg'
import viteLogo from '/vite.svg'

document.querySelector('#app').innerHTML = `
  <div>
    <a href="https://vitejs.dev" target="_blank">
      <img src="${viteLogo}" class="logo" alt="Vite logo" />
    </a>
    <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank">
      <img src="${javascriptLogo}" class="logo vanilla" alt="JavaScript logo" />
    </a>
    <h1>Hello Vite!</h1>
    <div class="card">
      <button id="counterRed" type="button">Red</button>
      <button id="counterBlue" type="button">Blue</button>
    </div>
    <p class="read-the-docs">
      Click on the Vite logo to learn more
    </p>
  </div>
`

const red = document.querySelector('#counterRed')
const blue = document.querySelector('#counterBlue')
const options = {
  method: "PUT",
  headers: {
    'Content-Type': 'application/json',
  }
};

red.addEventListener('click', () => {
  fetch('http://localhost:3000/red', options)
    .then(response => response.json())
    .then(response => {
      console.log(response)
    })

})

blue.addEventListener('click', () => {
  fetch('http://localhost:3000/blue', options)
    .then(response => response.json())
    .then(response => {
      console.log(response)
    })

})
